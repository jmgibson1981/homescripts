#!/bin/sh
# tadaen sylvermane | jason gibson
# my usual lxd container build options

# begin script #

if [ $(expr "$1" % 2) = 0 ] ; then
	lxc launch ubuntu:"$1".04 "$2"
	lxc config set "$2" boot.autostart true
	read -r -p "docker required? (y|n) -> " dockeranswer
	case "$dockeranswer" in
		y|yes|Yes|YES)
			lxc config set "$2" security.nesting true
			;;
	esac
	read -r -p "privilege required? (y|n) -> " privilege
	case "$privilege" in
		y|yes|Yes|YES)
			lxc config set "$2" security.privileged true
			;;
	esac
else
	echo "not going to help you with a non lts version!"
fi

# end script #
