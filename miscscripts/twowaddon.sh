#!/bin/sh

TWOWDIR="$HOME"/Documents/WINE/TurtleWoW/Interface/AddOns

update_all() {
    for i in * ; do
        if [ -d "$i"/.git ] ; then
            cd "$i" && git pull && cd ..
        fi
    done
}




cd "$TWOWDIR" && update_all
