#!/bin/sh
# tadaen sylvermane | jason gibson
# sql backup & restoration

# variables #

PATH="/usr/bin:/snap/bin"
SQLBACKUPS=/backups/mysqldump
NOW=$(date +%Y.%m.%d.%H.%M)

# functions #

old_backup_cleaner() {
	TEMPFILE=/tmp/dbcleanup.tmp
	for folderloc in "$@" ; do
		for existing in "$1"/* ; do
			echo "$existing" >> "$TEMPFILE"
		done
		if [ $(wc -l < "$TEMPFILE") -gt 5 ] ; then
			rm $(sort -n < "$TEMPFILE" | head -n 1)
		fi
		rm "$TEMPFILE"
		shift
	done
}

full_server_dump() {
	BACKUPDIR="$SQLBACKUPS"/"$1"
	for db in $(docker exec -t "$1" mysql -e "show databases;" \
	| awk '{print $2}') ; do
		FILENAME="$BACKUPDIR"/"$db"/"$db"."$NOW".sql.gz
		case "$db" in
			Database|information_schema|mysql|performance_schema)
				continue
				;;
			*)
				[ -d "$BACKUPDIR"/"$db" ] || mkdir -p "$BACKUPDIR"/"$db"
				docker exec -t "$1" mysqldump \
				--user=backup \
				--password=backup \
				--single-transaction "$db" \
				| gzip > "$FILENAME"
				;;
		esac
		old_backup_cleaner "$BACKUPDIR"/"$db"
	done
	# this should just backup the users and grants
	[ -d "$BACKUPDIR"/mysql ] || mkdir -p "$BACKUPDIR"/mysql
	docker exec -t "$1" mysql -BNe \
	"select concat('\'',user,'\'@\'',host,'\'') from mysql.user where user \
	!= 'root'" | while read uh ; do 
	docker exec -t "$1" mysql -BNe "show grants for $uh" \
	| sed 's/$/;/; s/\\\\/\\/g'; \
	done | gzip > "$BACKUPDIR"/mysql/mysql."$NOW".sql.gz
	rm "$BACKUPDIR"/mysql/mysql."$NOW".sql.gz
	old_backup_cleaner "$BACKUPDIR"/mysql
}

full_server_restore() {
	for db in $(find "$SQLBACKUPS"/"$1"/ -maxdepth 1 -mindepth 1 -type d) ; do
		DBNAME=$(basename "$db")
		docker exec -i "$1" mysql -e "CREATE DATABASE IF NOT EXISTS ${DBNAME};"
		zcat $(find "$db"/ -name '*.sql.gz' | sort -n | tail -n 1) \
		| docker exec -i "$1" mysql "$DBNAME"
	done
}

# begin script #

case "$1" in
	dump|restore)
		MAINOPT="$1"
		shift
		for DCON in "$@" ; do
			echo "$DCON"
			if docker ps | grep "$DCON" ; then
				full_server_"$MAINOPT" "$DCON"
			fi
		done
		;;
	*)
		echo "usage: ${0} (dump|restore) (dockercontainers)"
		exit 0
		;;
esac

# end script #
