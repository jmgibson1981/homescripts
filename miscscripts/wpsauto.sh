#!/bin/sh
# tadaen sylvermane | jason gibson
# connect to wps capable router via push button.
# but this does it via terminal window. just run the script.
# https://askubuntu.com/questions/120367/how-to-connect-to-wi-fi-ap-through-wps

# variables # 

WIFIIF=$(grep wpa /proc/net/unix | cut -d \/ -f 4)

# begin script #

# steps 1-3 https://askubuntu.com/a/170799

if [ "$USER" = root ] ; then
	# stop NetworkManager for this session
	systemctl stop NetworkManager
	# create wpa supplicant base file
	if [ ! -f /etc/wpa_supplicant.conf ] ; then
		echo "ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=0
update_config=1" > /etc/wpa_supplicant.conf
	fi
	# start wpa_supplicant daemon
	wpa_supplicant -B -Dwext -i"$WIFIIF" -c /etc/wpa_supplicant.conf
	# https://askubuntu.com/a/769600
	wpa_cli wps_pbc
	# go push the button!
	echo "push the wps button on the router now!"
	read -p "after wps button pushed type yes to connect or no to cancel -> " yesno
	case "$yesno" in
		Y|y|yes|Yes|YES)
			dhclient
			wait
			if ping -c 1 8.8.8.8 ; then
				echo "connected!"
				exit 0
			fi
			;;
		*)
			# cancel all above changes #
			rm /etc/wpa_supplicant.conf
			kill $(ps aux | grep "$WIFIIF" | grep root | awk '{print $2}')
			systemctl start NetworkManager
			exit 1
			;;
	esac
else
	echo "must run as root or with sudo"
	exit 1
fi

# end script #
