#!/bin/sh
# tadaen sylvermane | jason gibson
# nmcli helper

case "$1" in
	on|off)
		nmcli radio wifi "$1"
		;;
	scan)
		nmcli dev wifi list
		;;
	connect)
		nmcli dev wifi connect "$2" password "$3"
		sleep 10
		if ping -c 1 8.8.8.8 ; then
			echo "connected"
		else
			echo "failed to connect"
			echo "check ssid & password"
		fi	
		;;	
	*)
		echo "usage: ${0} on | off | scan | connect"
		echo "on = enable wifi radio"
		echo "off = disable wifi radio"
		echo "scan = show available wifi networks"
		echo "connect = connect to specified ssid"
		echo "connect format is: ${0} connect ssid password"
		;;
esac
		