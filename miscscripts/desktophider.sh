#!/bin/sh
# tadaen sylvermane | jason gibson
# global on directory to make all .desktop files NoDisplay
# normal usage would be to copy everything from /usr/share/applications
# to /usr/local/share/applications. run on /usr/local/share/applications
# then remove the ones i want visible from /usr/local/share/applications

# begin script #

for file in "$1"/*.desktop ; do
	DTENT=$(grep -n Desktop\ Entry "$file" | cut -d: -f 1)
	NLINE=$(expr "$DTENT" + 1)
	sed -i "${NLINE} i NoDisplay=true" "$file"
done

# end script #
