#!/bin/sh

# new system setup options

case "$1" in
	fonts)
		clear
		if ! grep -q FREETYPE_PROPERTIES /etc/environment ; then
			echo "FREETYPE_PROPERTIES=\"cff:no-stem-darkening=0 autofitter:no-stem-darkening=0\"" >> /etc/environment
			echo "\fonts settings applied. you will need to reboot to see the effect\n"
		else
			echo "\nfonts settings already active!\n"
		fi
		;;
esac
