#!/bin/sh

for i in $(ps aux | \
	   grep "$1" | \
	   grep -v grep | \
           awk '{print $2}') ; do
	kill -9 "$i"
done
