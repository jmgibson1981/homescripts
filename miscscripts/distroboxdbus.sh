#!/bin/sh
# distrobox create with dbus
# found @ https://unix.stackexchange.com/questions/778021/how-can-i-access-my-host-systems-system-dbus-bus-in-a-distrobox-container?newreg=88d1e0cbf0b647d38390a2e40e2c765a
distrobox create \
	--additional-flags "--env DBUS_SYSTEM_BUS_ADDRESS=unix:path=/run/host/var/run/dbus/system_bus_socket" \
	-n "$1" \
	-i "$2"
