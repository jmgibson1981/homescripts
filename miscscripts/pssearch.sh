#!/bin/sh

ps aux | \
	grep "$USER" | \
	grep -v '/usr/lib' | \
	grep -v '/usr/bin'
