#!/bin/sh
# tadaen sylvermane | jason gibson

# variables #

STORAGEFOLDER=/var/lib/ampache
#CRONTAB=/var/spool/cron/crontabs/root

# functions #

# sql server #

local_sql_config_func() {
	for file in $(find /var/lib"$STORAGEFOLDER"/sql/ -maxdepth 1 -mindepth 1) ; do
		case $(basename "$file") in
			mysql_upgrade_info|debian-10.3.flag)
				continue
				;;
			*)
				chown -R mysql:mysql "$file"
				;;
		esac
	done
	echo "[mysqld]
port = 3306
optimizer_search_depth = 1
skip-name-resolve
innodb_adaptive_hash_index = off
sort_buffer_size = 16M
datadir = ${STORAGEFOLDER}/sql
max_allowed_packet=256M" > /etc/mysql/mariadb.conf.d/99-ampache.cnf
	if [ ! -e "$STORAGEFOLDER"/sql/.tzdatalockDNR ] ; then
		mkdir -p "$STORAGEFOLDER"/sql
		chown -R mysql:mysql "$STORAGEFOLDER"/sql
		mysql_install_db --user=mysql --ldata="$STORAGEFOLDER"/sql/ --basedir=/usr
		/etc/init.d/mariadb start
		echo "ALTER USER 'root'@'localhost' IDENTIFIED BY 'ampache';
GRANT ALL ON *.* TO 'root'@'localhost';
FLUSH PRIVILEGES;
CREATE USER IF NOT EXISTS 'backup'@'localhost' IDENTIFIED BY 'backup';
GRANT ALL ON *.* TO 'backup'@'localhost';
FLUSH PRIVILEGES;" > /tmp/ampachedbsetup
		cat /tmp/ampachedbsetup | mysql
		mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql mysql
		touch "$STORAGEFOLDER"/sql/.tzdatalockDNR
		/etc/init.d/mariadb stop
	fi
}

local_sql_config_func

# clear stale pid
[ -f /run/apache2/apache2.pid ] && rm /run/apache2/apache2.pid
[ -f /run/cron.pid ] && rm /run/cron.pid
[ -f /run/mysqld/mysqld.pid ] && rm /run/mysqld/mysqld.pid
[ -f /run/mysqld/mysqld.sock ] && rm /run/mysqld/mysqld.sock

# set timezone here
echo "America/Phoenix" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata
rm /etc/localtime
ln -s /usr/share/zoneinfo/America/Phoenix /etc/localtime
# start required services
/etc/init.d/mariadb start
service cron start
service apache2 start
exec bash

# end script #
