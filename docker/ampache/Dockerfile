# Ampache #
FROM debian:11
LABEL maintainer="Tadaen Sylvermane <jmgibson81@gmail.com>"

# needed for tzdata
# https://stackoverflow.com/questions/44331836/apt-get-install-tzdata-noninteractive

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y \
	gnupg \
	curl && \
	[ -e /etc/localtime ] && rm /etc/localtime && \
	ln -s /usr/share/zoneinfo/$(curl https://ipapi.co/timezone) /etc/localtime

# entrypoint copy

COPY entrypoint.sh /entrypoint.sh

# install dependencies

RUN sed -i 's/main/main contrib non-free/g' /etc/apt/sources.list && \
	apt update && apt install -y \
	cron \
	ffmpeg \
	flac \
	gosu \
	inotify-tools \
	lame \
	libavcodec-extra \
	libev-libevent-dev \
	libfaac-dev \
	libmp3lame-dev \
	libtheora-dev \
	libvorbis-dev \
	libvpx-dev \
	php \
	php-curl \
	php-gd \
	php-intl \
	php-json \
	php-ldap \
	php-mysql \
	php-xml \
	php-zip \
	vorbis-tools \
	zip \
	unzip \
	git \
	mariadb-server \
	wget && \
	a2enmod rewrite

# composer, git, final permissions

RUN wget -O /usr/local/bin/composer https://getcomposer.org/download/latest-stable/composer.phar && \
	chmod 755 /usr/local/bin/composer && \
	mkdir -p /ampache && \
	git clone -b release5 https://github.com/ampache/ampache.git /ampache && \
	rm -rf /var/www/html && \
	ln -sf /ampache/public /var/www/html && \
	chown -R www-data:www-data /ampache && \
	cd /ampache && \
	composer install && \
	chmod 755 /entrypoint.sh

# entrypoint

ENTRYPOINT ["/entrypoint.sh"]
