#!/bin/sh
# tadaen sylvermane | jason gibson

# variables #

MAINFOLDER=/usr/share/zoneminder
MYSQLDBFOLDER=/var/lib/mysql
ZMEVENTSLOC=zmevents
ZMEVENTSFOLDER="$MYSQLDBFOLDER"/"$ZMEVENTSLOC"

# begin script #

# clean errant pids #

for pid in mysqld zm apache2 rsyslogd ; do
	find /var/run/ -type f -name "$pid".pid -exec rm {} \;
done

service rsyslog start

# set mysql configurations #

# create events directory if not exist #

[ -d "$ZMEVENTSFOLDER" ] || mkdir -p "$ZMEVENTSFOLDER"

for folder in cache events temp images ; do
	if [ -d "$ZMEVENTSFOLDER"/"$folder" ] ; then
		rm -r /var/cache/zoneminder/"$folder"
	else
		if [ -d /var/cache/zoneminder/"$folder" ] ; then
			mv /var/cache/zoneminder/"$folder" "$ZMEVENTSFOLDER"/
		else
			mkdir -p "$ZMEVENTSFOLDER"/"$folder"
		fi
	fi
	ln -s "$ZMEVENTSFOLDER"/"$folder" /var/cache/zoneminder/
done

# set permissions of all directories #

for file in $(find "$MYSQLDBFOLDER"/ -mindepth 1 -maxdepth 1) ; do
	FILEBASE=$(basename "$file")
	case "$FILEBASE" in
		debian-10.3.flag|mysql_upgrade_info)
			continue
			;;
		"$ZMEVENTSLOC")
			chown -R www-data:www-data "$MYSQLDBFOLDER"/"$FILEBASE"
			;;
		*)
			chown -R mysql:mysql "$MYSQLDBFOLDER"/"$FILEBASE"
			;;
	esac
done

# create configuration file #

echo "[mysqld]
datadir = ${MYSQLDBFOLDER}
skip-name-resolve
bind-address            = 0.0.0.0
port    = 53306
innodb_file_per_table = ON
innodb_buffer_pool_size = 256M
innodb_log_file_size = 32M" > /etc/mysql/mariadb.conf.d/99-optimize.cnf

# start mysql service #

service mysql start

# create timezone db entries #

if [ ! -e "$MYSQLDBFOLDER"/.tzdatalockDNR ] ; then
        mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql mysql
        touch "$MYSQLDBFOLDER"/.tzdatalockDNR
	SQLRESTART=1
fi

# create zm database and user if not exist else make sure permissions are correct #

if [ ! -d "$MYSQLDBFOLDER"/zm ] ; then
        cat "$MAINFOLDER"/db/zm_create.sql | mysql mysql
        mysql -e "create user 'zmuser'@localhost identified by 'zmpass';"
        mysql -e "grant all on zm.* to 'zmuser'@localhost;"
	mysql -e "flush privileges;"
	SQLRESTART=1
fi

# restart sql server if anything needs it #

[ -z "$SQLRESTART" ] && service mysql restart

# create zoneminder system groups if not exist #

adduser www-data video

# tell zoneminder to use internal mysql | maria db #

# added lock file to prevent modification on further container restarts

if [ ! -f /etc/zm/zm.conf.lock ] ; then
	sed -i 's/ZM_DB_HOST=localhost/ZM_DB_HOST=localhost:53306/' /etc/zm/zm.conf
	touch /etc/zm/zm.conf.lock
fi

# tell zoneminder to use different cache dir for single data volume on host #

sed -i 's/ZM_DIR_EVENTS/#ZM_DIR_EVENTS/' /etc/zm/conf.d/01-system-paths.conf
echo "ZM_DIR_EVENTS=${ZMEVENTSFOLDER}" >> /etc/zm/conf.d/01-system-paths.conf

# start zoneminder #

service zoneminder start

# reconfigure listening ports for apache2 #

if [ ! -e /etc/apache2/ports.conf.bak ] ; then
	mv /etc/apache2/ports.conf /etc/apache2/ports.conf.bak
	echo "Listen 50080
<IfModule ssl_module>
	Listen 50443
</IfModule>
<IfModule mod_gnutls.c>
	Listen 50443
</IfModule>" > /etc/apache2/ports.conf
fi

# enable apache settings #

service apache2 start
a2enconf zoneminder
a2enmod headers
a2enmod expires
a2enmod rewrite
service apache2 reload

# be sure finished #

wait

# run bash to keep it going #

bash

# end script #
