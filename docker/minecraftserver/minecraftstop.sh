#/bin/sh
# tadaen sylvermane | jason gibson
# shut down minecraft servers gracefully on system shutdown / reboot

# test version. no longer used as systemd service does it

# local PATH

PATH="/snap/bin:/usr/bin"

# begin script #

if docker ps | grep "$1" ; then
	docker exec "$1" /etc/init.d/minecraft stop
	wait
	docker stop "$1"
fi

# end script #
