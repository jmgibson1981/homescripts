#!/bin/sh
# tadaen sylvermane | jason gibson

# variables #

STORAGE=/var/minidlna

# begin script #

# eliminate stuck pid file if needed #

[ -e /run/minidlna/minidlna.pid ] && rm -f /run/minidlna/minidlna.pid

# create folders as needed and make sure permissions are correct #

for folder in db log ; do
	if [ ! -d "$STORAGE"/"$folder" ] ; then
		mkdir -p "$STORAGE"/"$folder"
	fi
	chown -R minidlna:minidlna "$STORAGE"/"$folder"
done

# start service #

service minidlna start
exec bash
